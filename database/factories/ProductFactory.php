<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type_product_id' => $this->faker->randomDigitNotNull,
        'name' => $this->faker->word,
        'prix' => $this->faker->randomDigitNotNull,
        'adresse' => $this->faker->word,
        'quantite' => $this->faker->randomDigitNotNull,
        'type_quantite' => $this->faker->word,
        'description' => $this->faker->text,
        'image' => $this->faker->word,
        'categorie_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
