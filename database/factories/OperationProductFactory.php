<?php

namespace Database\Factories;

use App\Models\OperationProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

class OperationProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OperationProduct::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'prix' => $this->faker->randomDigitNotNull,
        'quantite' => $this->faker->randomDigitNotNull,
        'description' => $this->faker->text,
        'operation_id' => $this->faker->randomDigitNotNull,
        'product_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
