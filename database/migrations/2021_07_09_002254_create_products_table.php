<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_product_id')->unsigned();
            $table->string('name');
            $table->integer('prix');
            $table->string('adresse');
            $table->integer('quantite');
            $table->string('type_quantite');
            $table->text('description');
            $table->string('image');
            $table->integer('categorie_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('type_product_id')->references('id')->on('type_products');
            $table->foreign('categorie_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
