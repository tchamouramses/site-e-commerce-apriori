<?php

namespace App\Repositories;

use App\Models\TypeOperation;
use App\Repositories\BaseRepository;

/**
 * Class TypeOperationRepository
 * @package App\Repositories
 * @version July 9, 2021, 12:23 am UTC
*/

class TypeOperationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeOperation::class;
    }
}
