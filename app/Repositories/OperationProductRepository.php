<?php

namespace App\Repositories;

use App\Models\OperationProduct;
use App\Repositories\BaseRepository;

/**
 * Class OperationProductRepository
 * @package App\Repositories
 * @version July 9, 2021, 12:24 am UTC
*/

class OperationProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'prix',
        'quantite',
        'description',
        'operation_id',
        'product_id',
        'created_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OperationProduct::class;
    }
}
