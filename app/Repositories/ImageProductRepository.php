<?php

namespace App\Repositories;

use App\Models\ImageProduct;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;


/**
 * Class ImageProductRepository
 * @package App\Repositories
 * @version July 9, 2021, 12:30 am UTC
 */

class ImageProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'image',
        'product_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ImageProduct::class;
    }

    public function createImage(Request $request)
    {

        $path = " ";
        //upload image
        if (isset($request->image)) {
            $image = $request->file('image');
            if ($image != null) {
                $extension = $image->getClientOriginalExtension();
                $relativeDestination = "uploads/Image";
                $destinationPath = public_path($relativeDestination);
                $safeName = "Image" . time() . '.' . $extension;
                $image->move($destinationPath, $safeName);
                $path = "$relativeDestination/$safeName";
            }
        }

        $input = $request->all();
        $input['image'] = $path;
        return $this->create($input);
    }

    public function updateImage(Request $request, $id)
    {

        $path = " ";
        //upload image
        if (isset($request->image)) {
            $image = $request->file('image');
            if ($image != null) {
                $extension = $image->getClientOriginalExtension();
                $relativeDestination = "uploads/Image";
                $destinationPath = public_path($relativeDestination);
                $safeName = "Image" . time() . '.' . $extension;
                $image->move($destinationPath, $safeName);
                $path = "$relativeDestination/$safeName";
            }
        }

        $input = $request->all();
        $input['image'] = $path;
        return $this->update($input, $id);
    }
}
