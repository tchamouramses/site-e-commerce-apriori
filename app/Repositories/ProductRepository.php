<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;


/**
 * Class ProductRepository
 * @package App\Repositories
 * @version July 9, 2021, 12:22 am UTC
 */

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type_product_id',
        'name',
        'prix',
        'adresse',
        'quantite',
        'type_quantite',
        'description',
        'image',
        'categorie_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }

    public function createProduct(Request $request)
    {

        $path = " ";
        //upload image
        if (isset($request->image)) {
            $image = $request->file('image');
            if ($image != null) {
                $extension = $image->getClientOriginalExtension();
                $relativeDestination = "uploads/Product";
                $destinationPath = public_path($relativeDestination);
                $safeName = "Product" . time() . '.' . $extension;
                $image->move($destinationPath, $safeName);
                $path = "$relativeDestination/$safeName";
            }
        }

        $input = $request->all();
        $input['image'] = $path;
        return $this->create($input);
    }

    public function updateProduct(Request $request, $id)
    {

        $path = " ";
        //upload image
        if (isset($request->image)) {
            $image = $request->file('image');
            if ($image != null) {
                $extension = $image->getClientOriginalExtension();
                $relativeDestination = "uploads/Product";
                $destinationPath = public_path($relativeDestination);
                $safeName = "Product" . time() . '.' . $extension;
                $image->move($destinationPath, $safeName);
                $path = "$relativeDestination/$safeName";
            }
        }

        $input = $request->all();
        $input['image'] = $path;
        return $this->update($input, $id);
    }
}
