<?php

namespace App\Providers;
use App\Models\Product;
use App\Models\Operation;
use App\Models\TypeOperation;
use App\Models\Category;
use App\Models\TypeProduct;

use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['image_products.fields'], function ($view) {
            $productItems = Product::pluck('name','id')->toArray();
            $view->with('productItems', $productItems);
        });
        View::composer(['images.fields'], function ($view) {
            $productItems = Product::pluck('name','id')->toArray();
            $view->with('productItems', $productItems);
        });
        View::composer(['operation_products.fields'], function ($view) {
            $productItems = Product::pluck('name','id')->toArray();
            $view->with('productItems', $productItems);
        });
        View::composer(['operation_products.fields'], function ($view) {
            $operationItems = Operation::pluck('name','id')->toArray();
            $view->with('operationItems', $operationItems);
        });
        View::composer(['operations.fields'], function ($view) {
            $type_operationItems = TypeOperation::pluck('name','id')->toArray();
            $view->with('type_operationItems', $type_operationItems);
        });
        View::composer(['products.fields'], function ($view) {
            $categoryItems = Category::pluck('name','id')->toArray();
            $view->with('categoryItems', $categoryItems);
        });
        View::composer(['products.fields'], function ($view) {
            $type_productItems = TypeProduct::pluck('name','id')->toArray();
            $view->with('type_productItems', $type_productItems);
        });
        //
    }
}