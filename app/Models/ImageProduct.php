<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ImageProduct
 * @package App\Models
 * @version July 9, 2021, 12:30 am UTC
 *
 * @property string $name
 * @property string $image
 * @property integer $product_id
 */
class ImageProduct extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'image_products';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'image',
        'product_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'image' => 'string',
        'product_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'image' => 'required',
        'product_id' => 'required'
    ];

    
}
