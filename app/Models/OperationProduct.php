<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class OperationProduct
 * @package App\Models
 * @version July 9, 2021, 12:24 am UTC
 *
 * @property integer $prix
 * @property integer $quantite
 * @property string $description
 * @property integer $operation_id
 * @property integer $product_id
 * @property string $created_at
 */
class OperationProduct extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'operation_products';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'prix',
        'quantite',
        'description',
        'operation_id',
        'product_id',
        'created_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'prix' => 'integer',
        'quantite' => 'integer',
        'description' => 'string',
        'operation_id' => 'integer',
        'product_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'prix' => 'required',
        'quantite' => 'required',
        'operation_id' => 'required',
        'product_id' => 'required'
    ];

    
}
