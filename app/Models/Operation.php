<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Operation
 * @package App\Models
 * @version July 9, 2021, 12:23 am UTC
 *
 * @property string $name
 * @property string $description
 * @property integer $type_operation_id
 * @property string $created_at
 */
class Operation extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'operations';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description',
        'type_operation_id',
        'created_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'type_operation_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'type_operation_id' => 'required'
    ];

    
}
