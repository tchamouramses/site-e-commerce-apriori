<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Product
 * @package App\Models
 * @version July 9, 2021, 12:22 am UTC
 *
 * @property integer $type_product_id
 * @property string $name
 * @property integer $prix
 * @property string $adresse
 * @property integer $quantite
 * @property string $type_quantite
 * @property string $description
 * @property string $image
 * @property integer $categorie_id
 */
class Product extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'products';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'type_product_id',
        'name',
        'prix',
        'adresse',
        'quantite',
        'type_quantite',
        'description',
        'image',
        'categorie_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type_product_id' => 'integer',
        'name' => 'string',
        'prix' => 'integer',
        'adresse' => 'string',
        'quantite' => 'integer',
        'type_quantite' => 'string',
        'description' => 'string',
        'image' => 'string',
        'categorie_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type_product_id' => 'required',
        'name' => 'required',
        'prix' => 'required',
        'categorie_id' => 'required'
    ];

    
}
