<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Image
 * @package App\Models
 * @version July 9, 2021, 12:25 am UTC
 *
 * @property string $name
 * @property integer $product_id
 */
class Image extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'images';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'product_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'product_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'product_id' => 'required'
    ];

    
}
