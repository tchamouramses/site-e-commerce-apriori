<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypeProductRequest;
use App\Http\Requests\UpdateTypeProductRequest;
use App\Repositories\TypeProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TypeProductController extends AppBaseController
{
    /** @var  TypeProductRepository */
    private $typeProductRepository;

    public function __construct(TypeProductRepository $typeProductRepo)
    {
        $this->typeProductRepository = $typeProductRepo;
    }

    /**
     * Display a listing of the TypeProduct.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $typeProducts = $this->typeProductRepository->paginate(10);

        return view('type_products.index')
            ->with('typeProducts', $typeProducts);
    }

    /**
     * Show the form for creating a new TypeProduct.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_products.create');
    }

    /**
     * Store a newly created TypeProduct in storage.
     *
     * @param CreateTypeProductRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeProductRequest $request)
    {
        $input = $request->all();

        $typeProduct = $this->typeProductRepository->create($input);

        Flash::success('Type Product saved successfully.');

        return redirect(route('typeProducts.index'));
    }

    /**
     * Display the specified TypeProduct.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeProduct = $this->typeProductRepository->find($id);

        if (empty($typeProduct)) {
            Flash::error('Type Product not found');

            return redirect(route('typeProducts.index'));
        }

        return view('type_products.show')->with('typeProduct', $typeProduct);
    }

    /**
     * Show the form for editing the specified TypeProduct.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeProduct = $this->typeProductRepository->find($id);

        if (empty($typeProduct)) {
            Flash::error('Type Product not found');

            return redirect(route('typeProducts.index'));
        }

        return view('type_products.edit')->with('typeProduct', $typeProduct);
    }

    /**
     * Update the specified TypeProduct in storage.
     *
     * @param int $id
     * @param UpdateTypeProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeProductRequest $request)
    {
        $typeProduct = $this->typeProductRepository->find($id);

        if (empty($typeProduct)) {
            Flash::error('Type Product not found');

            return redirect(route('typeProducts.index'));
        }

        $typeProduct = $this->typeProductRepository->update($request->all(), $id);

        Flash::success('Type Product updated successfully.');

        return redirect(route('typeProducts.index'));
    }

    /**
     * Remove the specified TypeProduct from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeProduct = $this->typeProductRepository->find($id);

        if (empty($typeProduct)) {
            Flash::error('Type Product not found');

            return redirect(route('typeProducts.index'));
        }

        $this->typeProductRepository->delete($id);

        Flash::success('Type Product deleted successfully.');

        return redirect(route('typeProducts.index'));
    }
}
