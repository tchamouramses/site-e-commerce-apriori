<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOperationProductRequest;
use App\Http\Requests\UpdateOperationProductRequest;
use App\Repositories\OperationProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OperationProductController extends AppBaseController
{
    /** @var  OperationProductRepository */
    private $operationProductRepository;

    public function __construct(OperationProductRepository $operationProductRepo)
    {
        $this->operationProductRepository = $operationProductRepo;
    }

    /**
     * Display a listing of the OperationProduct.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $operationProducts = $this->operationProductRepository->paginate(10);

        return view('operation_products.index')
            ->with('operationProducts', $operationProducts);
    }

    /**
     * Show the form for creating a new OperationProduct.
     *
     * @return Response
     */
    public function create()
    {
        return view('operation_products.create');
    }

    /**
     * Store a newly created OperationProduct in storage.
     *
     * @param CreateOperationProductRequest $request
     *
     * @return Response
     */
    public function store(CreateOperationProductRequest $request)
    {
        $input = $request->all();

        $operationProduct = $this->operationProductRepository->create($input);

        Flash::success('Operation Product saved successfully.');

        return redirect(route('operationProducts.index'));
    }

    /**
     * Display the specified OperationProduct.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $operationProduct = $this->operationProductRepository->find($id);

        if (empty($operationProduct)) {
            Flash::error('Operation Product not found');

            return redirect(route('operationProducts.index'));
        }

        return view('operation_products.show')->with('operationProduct', $operationProduct);
    }

    /**
     * Show the form for editing the specified OperationProduct.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $operationProduct = $this->operationProductRepository->find($id);

        if (empty($operationProduct)) {
            Flash::error('Operation Product not found');

            return redirect(route('operationProducts.index'));
        }

        return view('operation_products.edit')->with('operationProduct', $operationProduct);
    }

    /**
     * Update the specified OperationProduct in storage.
     *
     * @param int $id
     * @param UpdateOperationProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOperationProductRequest $request)
    {
        $operationProduct = $this->operationProductRepository->find($id);

        if (empty($operationProduct)) {
            Flash::error('Operation Product not found');

            return redirect(route('operationProducts.index'));
        }

        $operationProduct = $this->operationProductRepository->update($request->all(), $id);

        Flash::success('Operation Product updated successfully.');

        return redirect(route('operationProducts.index'));
    }

    /**
     * Remove the specified OperationProduct from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $operationProduct = $this->operationProductRepository->find($id);

        if (empty($operationProduct)) {
            Flash::error('Operation Product not found');

            return redirect(route('operationProducts.index'));
        }

        $this->operationProductRepository->delete($id);

        Flash::success('Operation Product deleted successfully.');

        return redirect(route('operationProducts.index'));
    }
}
