<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateImageProductRequest;
use App\Http\Requests\UpdateImageProductRequest;
use App\Repositories\ImageProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ImageProductController extends AppBaseController
{
    /** @var  ImageProductRepository */
    private $imageProductRepository;

    public function __construct(ImageProductRepository $imageProductRepo)
    {
        $this->imageProductRepository = $imageProductRepo;
    }

    /**
     * Display a listing of the ImageProduct.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $imageProducts = $this->imageProductRepository->paginate(10);
        foreach ($imageProducts as $gal) {
            $gal->image = url($gal->image);
        }


        return view('image_products.index')
            ->with('imageProducts', $imageProducts);
    }

    /**
     * Show the form for creating a new ImageProduct.
     *
     * @return Response
     */
    public function create()
    {
        return view('image_products.create');
    }

    /**
     * Store a newly created ImageProduct in storage.
     *
     * @param CreateImageProductRequest $request
     *
     * @return Response
     */
    public function store(CreateImageProductRequest $request)
    {
        //$input = $request->all();

        $imageProduct = $this->imageProductRepository->createImage($request);

        Flash::success('Image Product saved successfully.');

        return redirect(route('imageProducts.index'));
    }

    /**
     * Display the specified ImageProduct.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $imageProduct = $this->imageProductRepository->find($id);

        if (empty($imageProduct)) {
            Flash::error('Image Product not found');

            return redirect(route('imageProducts.index'));
        }

        return view('image_products.show')->with('imageProduct', $imageProduct);
    }

    /**
     * Show the form for editing the specified ImageProduct.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $imageProduct = $this->imageProductRepository->find($id);

        if (empty($imageProduct)) {
            Flash::error('Image Product not found');

            return redirect(route('imageProducts.index'));
        }

        return view('image_products.edit')->with('imageProduct', $imageProduct);
    }

    /**
     * Update the specified ImageProduct in storage.
     *
     * @param int $id
     * @param UpdateImageProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImageProductRequest $request)
    {
        $imageProduct = $this->imageProductRepository->find($id);

        if (empty($imageProduct)) {
            Flash::error('Image Product not found');

            return redirect(route('imageProducts.index'));
        }

        $imageProduct = $this->imageProductRepository->updateImage($request, $id);

        Flash::success('Image Product updated successfully.');

        return redirect(route('imageProducts.index'));
    }

    /**
     * Remove the specified ImageProduct from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $imageProduct = $this->imageProductRepository->find($id);

        if (empty($imageProduct)) {
            Flash::error('Image Product not found');

            return redirect(route('imageProducts.index'));
        }

        $this->imageProductRepository->delete($id);

        Flash::success('Image Product deleted successfully.');

        return redirect(route('imageProducts.index'));
    }
}
