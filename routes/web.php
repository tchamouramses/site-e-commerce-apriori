<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('categories', App\Http\Controllers\CategoryController::class);

Route::resource('typeProducts', App\Http\Controllers\TypeProductController::class);

Route::resource('products', App\Http\Controllers\ProductController::class);

Route::resource('typeOperations', App\Http\Controllers\TypeOperationController::class);

Route::resource('operations', App\Http\Controllers\OperationController::class);

Route::resource('operationProducts', App\Http\Controllers\OperationProductController::class);

Route::resource('images', App\Http\Controllers\ImageController::class);

Route::resource('imageProducts', App\Http\Controllers\ImageProductController::class);




Route::view('contact', 'pullo/contact');
