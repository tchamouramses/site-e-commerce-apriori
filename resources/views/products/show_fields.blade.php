<!-- Type Product Id Field -->
<div class="col-sm-12">
    {!! Form::label('type_product_id', 'Type Product Id:') !!}
    <p>{{ $product->type_product_id }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $product->name }}</p>
</div>

<!-- Prix Field -->
<div class="col-sm-12">
    {!! Form::label('prix', 'Prix:') !!}
    <p>{{ $product->prix }}</p>
</div>

<!-- Adresse Field -->
<div class="col-sm-12">
    {!! Form::label('adresse', 'Adresse:') !!}
    <p>{{ $product->adresse }}</p>
</div>

<!-- Quantite Field -->
<div class="col-sm-12">
    {!! Form::label('quantite', 'Quantite:') !!}
    <p>{{ $product->quantite }}</p>
</div>

<!-- Type Quantite Field -->
<div class="col-sm-12">
    {!! Form::label('type_quantite', 'Type Quantite:') !!}
    <p>{{ $product->type_quantite }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $product->description }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', 'Image:') !!}
    <p><img height="200" src="{{ $product->image }}" alt=""></p>
</div>

<!-- Categorie Id Field -->
<div class="col-sm-12">
    {!! Form::label('categorie_id', 'Categorie Id:') !!}
    <p>{{ $product->categorie_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $product->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $product->updated_at }}</p>
</div>