<div class="header_section header_main">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="logo"><a href="#"><img src="images/logo.png"></a></div>
            </div>
            <div class="col-sm-9">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Basculer la navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav">
                            <a class="nav-item nav-link" href="index.html">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Domicile</font>
                                </font>
                            </a>
                            <a class="nav-item nav-link" href="collection.html">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Collection</font>
                                </font>
                            </a>
                            <a class="nav-item nav-link" href="shoes.html">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Chaussures</font>
                                </font>
                            </a>
                            <a class="nav-item nav-link" href="racing boots.html">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Bottes de course</font>
                                </font>
                            </a>
                            <a class="nav-item nav-link" href="contact.html">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Contacter</font>
                                </font>
                            </a>
                            <a class="nav-item nav-link last" href="#"><img src="images/search_icon.png"></a>
                            <a class="nav-item nav-link last" href="contact.html"><img src="images/shop_icon.png"></a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>