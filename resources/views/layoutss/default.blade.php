<!DOCTYPE html>
<html lang="fr" class="translated-ltr">

<head>
	<!-- basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- mobile metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<!-- site metas -->
	<title>Collection</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- bootstrap css -->
	<link rel="stylesheet" href="/pullo/css/bootstrap.min.css">

	<!-- style css -->
	<link rel="stylesheet" href="/pullo/css/style.css">
	<!-- Responsive-->
	<link rel="stylesheet" href="/pullo/css/responsive.css">
	<!-- fevicon -->
	<link rel="icon" href="/pullo/images/favicon.png" type="image/gif">
	<!-- Scrollbar Custom CSS -->
	<link rel="stylesheet" href="/pullo/css/jquery.mCustomScrollbar.min.css">
	<!-- Tweaks for older IEs-->
	<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
	<!-- owl stylesheets -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
	<style type="text/css">
		.fancybox-margin {
			margin-right: 15px;
		}
	</style>
	<link type="text/css" rel="stylesheet" charset="UTF-8" href="https://translate.googleapis.com/translate_static/css/translateelement.css">
</head>
<!-- body -->

<body class="main-layout">
	<!-- header section start -->
	<div class="header_section header_main">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="logo"><a href="#"><img src="/pullo/images/logo.png"></a></div>
				</div>
				<div class="col-sm-9">
					<nav class="navbar navbar-expand-lg navbar-light bg-light">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Basculer la navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
							<div class="navbar-nav">
								<a class="nav-item nav-link" href="index.html">
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Domicile</font>
									</font>
								</a>
								<a class="nav-item nav-link" href="collection.html">
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Collection</font>
									</font>
								</a>
								<a class="nav-item nav-link" href="shoes.html">
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Chaussures</font>
									</font>
								</a>
								<a class="nav-item nav-link" href="racing boots.html">
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Bottes de course</font>
									</font>
								</a>
								<a class="nav-item nav-link" href="contact.html">
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Contacter</font>
									</font>
								</a>
								<a class="nav-item nav-link last" href="#"><img src="/pullo/images/search_icon.png"></a>
								<a class="nav-item nav-link last" href="contact.html"><img src="/pullo/images/shop_icon.png"></a>
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<!-- new collection section start -->
	<!--/ Header end -->

	<!-- Banner area start -->
	@yield('banner')

	<!-- Banner area en d -->

	<div>
		@yield('content')
	</div>
	<!-- new collection section end -->
	<!-- section footer start -->
	<div class="section_footer">
		<div class="container">
			<div class="mail_section">
				<div class="row">
					<div class="col-sm-6 col-lg-2">
						<div><a href="#"><img src="images/footer-logo.png"></a></div>
					</div>
					<div class="col-sm-6 col-lg-2">
						<div class="footer-logo"><img src="images/phone-icon.png"><span class="map_text">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">(71) 1234567890</font>
								</font>
							</span></div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="footer-logo"><img src="images/email-icon.png"><span class="map_text">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Demo@gmail.com</font>
								</font>
							</span></div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="social_icon">
							<ul>
								<li><a href="#"><img src="images/fb-icon.png"></a></li>
								<li><a href="#"><img src="images/twitter-icon.png"></a></li>
								<li><a href="#"><img src="images/in-icon.png"></a></li>
								<li><a href="#"><img src="images/google-icon.png"></a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2"></div>
				</div>
			</div>
			<div class="footer_section_2">
				<div class="row">
					<div class="col-sm-4 col-lg-2">
						<p class="dummy_text">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;"> ipsum dolor sit amet, consectetur ipsum dolor sit amet, consectetur ipsum dolor sit amet,</font>
							</font>
						</p>
					</div>
					<div class="col-sm-4 col-lg-2">
						<h2 class="shop_text">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;">Adresse </font>
							</font>
						</h2>
						<div class="image-icon"><img src="images/map-icon.png"><span class="pet_text">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">No 40 Baria Sreet 15/2 New York City, NY, États-Unis.</font>
								</font>
							</span></div>
					</div>
					<div class="col-sm-4 col-md-6 col-lg-3">
						<h2 class="shop_text">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;">Notre compagnie </font>
							</font>
						</h2>
						<div class="delivery_text">
							<ul>
								<li>
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Livraison</font>
									</font>
								</li>
								<li>
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Mention légale</font>
									</font>
								</li>
								<li>
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">À propos de nous</font>
									</font>
								</li>
								<li>
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">paiement sécurisé</font>
									</font>
								</li>
								<li>
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Contactez-nous</font>
									</font>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<h2 class="adderess_text">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;">Des produits</font>
							</font>
						</h2>
						<div class="delivery_text">
							<ul>
								<li>
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Les prix baissent</font>
									</font>
								</li>
								<li>
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Nouveaux produits</font>
									</font>
								</li>
								<li>
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Meilleures ventes</font>
									</font>
								</li>
								<li>
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Contactez-nous</font>
									</font>
								</li>
								<li>
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">Plan du site</font>
									</font>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 col-lg-2">
						<h2 class="adderess_text">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;">Bulletin</font>
							</font>
						</h2>
						<div class="form-group">
							<input type="text" class="enter_email" placeholder="Entrer votre Email" name="Name">
						</div>
						<button class="subscribr_bt">
							<font style="vertical-align: inherit;">
								<font style="vertical-align: inherit;">S'abonner</font>
							</font>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- section footer end -->
	<div class="copyright">
		<font style="vertical-align: inherit;">
			<font style="vertical-align: inherit;">2019 Tous droits réservés. </font>
		</font><a href="https://html.design">
			<font style="vertical-align: inherit;">
				<font style="vertical-align: inherit;">Modèles HTML gratuits</font>
			</font>
		</a>
	</div>


	<!-- Javascript files-->
	<script src="/pullo/js/jquery.min.js"></script>
	<script src="/pullo/js/popper.min.js"></script>
	<script src="/pullo/js/bootstrap.bundle.min.js"></script>
	<script src="/pullo/js/jquery-3.0.0.min.js"></script>
	<script src="/pullo/js/plugin.js"></script>
	<!-- sidebar -->
	<script src="/pullo/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="/pullo/js/custom.js"></script>
	<!-- javascript -->
	<script src="/pullo/js/owl.carousel.js"></script>
	<script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
	<script>
		$(document).ready(function() {
			$(".fancybox").fancybox({
				openEffect: "none",
				closeEffect: "none"
			});


			$('#myCarousel').carousel({
				interval: false
			});

			//scroll slides on swipe for touch enabled devices

			$("#myCarousel").on("touchstart", function(event) {

				var yClick = event.originalEvent.touches[0].pageY;
				$(this).one("touchmove", function(event) {

					var yMove = event.originalEvent.touches[0].pageY;
					if (Math.floor(yClick - yMove) > 1) {
						$(".carousel").carousel('next');
					} else if (Math.floor(yClick - yMove) < -1) {
						$(".carousel").carousel('prev');
					}
				});
				$(".carousel").on("touchend", function() {
					$(this).off("touchmove");
				});
			});
		});
	</script>
	<div id="goog-gt-tt" class="skiptranslate" dir="ltr">
		<div style="padding: 8px;">
			<div>
				<div class="logo"><img src="https://www.gstatic.com/images/branding/product/1x/translate_24dp.png" width="20" height="20" alt="Google Traduction"></div>
			</div>
		</div>
		<div class="top" style="padding: 8px; float: left; width: 100%;">
			<h1 class="title gray">Texte d'origine</h1>
		</div>
		<div class="middle" style="padding: 8px;">
			<div class="original-text"></div>
		</div>
		<div class="bottom" style="padding: 8px;">
			<div class="activity-links"><span class="activity-link">Proposer une meilleure traduction</span><span class="activity-link"></span></div>
			<div class="started-activity-container">
				<hr style="color: #CCC; background-color: #CCC; height: 1px; border: none;">
				<div class="activity-root"></div>
			</div>
		</div>
		<div class="status-message" style="display: none;"></div>
	</div>


	<div class="goog-te-spinner-pos">
		<div class="goog-te-spinner-animation"><svg xmlns="http://www.w3.org/2000/svg" class="goog-te-spinner" width="96px" height="96px" viewBox="0 0 66 66">
				<circle class="goog-te-spinner-path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
			</svg></div>
	</div>
</body>

</html>