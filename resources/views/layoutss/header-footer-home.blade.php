<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Marathon Dschang</title>
      
        <!-- mobile responsive meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <!-- ** Plugins Needed for the Project ** -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="/assets/frontupdate/plugins/bootstrap/bootstrap.min.css">
          <!-- FontAwesome -->
        <link rel="stylesheet" href="/assets/frontupdate/plugins/fontawesome/font-awesome.min.css">
          <!-- Animation -->
          <link rel="stylesheet" href="/assets/frontupdate/plugins/animate.css">
          <!-- Prettyphoto -->
          <link rel="stylesheet" href="/assets/frontupdate/plugins/prettyPhoto.css">
          <!-- Owl Carousel -->
          <link rel="stylesheet" href="/assets/frontupdate/plugins/owl/owl.carousel.css">
          <link rel="stylesheet" href="/assets/frontupdate/plugins/owl/owl.theme.css">
          <!-- Flexslider -->
          <link rel="stylesheet" href="/assets/frontupdate/plugins/flex-slider/flexslider.css">
          <!-- Flexslider -->
          <link rel="stylesheet" href="/assets/frontupdate/plugins/cd-hero/cd-hero.css">
          <!-- Style Swicther -->
          <link id="style-switch" href="/assets/frontupdate/css/presets/preset3.css" media="screen" rel="stylesheet" type="text/css">
          <link rel="stylesheet" href="/assets/frontupdate/css/presets/home.css">
          <link rel="stylesheet" href="/assets/frontupdate/css/presets/countdown.css">

          <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
          <!--[if lt IE 9]>
            <script src="plugins/html5shiv.js"></script>
            <script src="plugins/respond.min.js"></script>
          <![endif]-->
      
        <!-- Main Stylesheet -->
        <link href="/assets/frontupdate/css/style.css" rel="stylesheet">
        
        <!--Favicon-->
          <link rel="icon" href="img/favicon/favicon-32x32.png" type="image/x-icon" />
          <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon/favicon-144x144.png">
          <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon/favicon-72x72.png">
          <link rel="apple-touch-icon-precomposed" href="img/favicon/favicon-54x54.png">
      
    </head>
    <body>
        
        <!-- Body inner start -->
        <div class="body-inner">
            
            <!-- Header start -->
            <header id="header" class="fixed-top header4" role="banner">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light px-4 py-2">
                        <a class="navbar-brand" href="{{ url('/') }}"><img class="img-fluid" src="/assets/frontupdate/images/logo2.jpeg" alt="logo"></a>
                        <button class="navbar-toggler ml-auto border-0 rounded-0 text-dark" type="button" data-toggle="collapse"
                            data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="fa fa-bars"></span>
                        </button>

                        <div class="collapse navbar-collapse text-center" id="navigation">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item dropdown active">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                        La course
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{ url('course/presentation') }}">Presentation</a>
                                        <a class="dropdown-item" href="{{ url('course/semi-marathon') }}">Semi-marathon</a>
                                        <a class="dropdown-item" href="{{ url('recompenses1') }}">Recompenses</a>
                                        <a class="dropdown-item" href="{{ url('programme-et-deroulement1') }}">Programme et deroulement</a>
                                        <a class="dropdown-item" href="{{ url('videos1') }}">Visionner les parcours</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('blog')}}">Actualités</a></a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                        Infos pratiques
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{ url('bon-a-savoir1') }}">A savoir avant de s’inscrire</a>
                                        <a class="dropdown-item" href="{{ url('prevention-sante1') }}">Prevention sante</a>
                                        <a class="dropdown-item" href="{{ url('Infos-Pratiques/incription-tarif') }}">Inscriptions et tarifs</a>
                                        <a class="dropdown-item" href="{{ url('village-du-semi-marathon1') }}">Village du semi-marathon</a>
                                        <a class="dropdown-item" href="{{ url('course/podium-primes') }}">Podium et prime</a>
                                        <a class="dropdown-item" href="{{ url('organisation-et-reglements1') }}">Organisation/Reglement</a>
                                        <a class="dropdown-item" href="{{ url('premier-semi-marathon1') }}">Mon premier semi-marathon</a>
                                    </div>
                                </li>
                               
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('contact1') }}">Contacts</a></a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                        Plus+
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{ url('gallerie') }}">Galerie</a>
                                        <a class="dropdown-item" href="{{ url('faq') }}">FAQ</a>
                                        <a class="dropdown-item" href="{{ url('partenaires') }}">Partenaires</a>
                                        <a class="dropdown-item" href="{{ url('decouvrir-dschang') }}">Decouvrez Dschang</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('inscription1') }}">Paticiper</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </header>
            <!--/ Header end -->
            @yield('banner')
            <!-- Slider start -->
            
            <div>
        	@yield('content')
		    </div>
        <!-- Footer start -->
        <footer id="footer" class="footer">
            <div class="container">
                   <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="footer-social unstyled">
                            <li>
                                <a title="Facebook" href="#">
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
                                </a>
                                <a title="Google+" href="#">
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-youtube"></i></span>
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-instagram"></i></span>
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-skype"></i></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="copyright-info"></div>
                <div class="row">
                    <div class="col-md-4 col-sm-12 footer-widget">
                        <h3 class="widget-title">A propos de nous</h3>
                        <figure> <img src="/assets/frontupdate/images/cafd.jpeg" style="margin-left: 25%" alt="image desctiption" width="50%">
                        </figure>
                        <div class="latest-post-items media">
                            <div class="latest-post-content media-body">
                                <h4><a href="#">CAFD:</a></h4>
                                <p class="post-meta">
                                    L'idée de la «Course de la
                                    Solidarité» est de faire vivre le groupement; faire connaître la falaise de Foréké-Dschang
                                    Cercle d’Action Foréké-Dschang, promotion, rayonnement et développement du groupement.
                                    afin de promouvoir notre
                                    culture à travers nos danses folkloriques.
                                    C’est aussi un événement de rencontre entre les filles, fils et sympathisants
                                    Foréké-Dschang. Bref un moment de
                                    retrouvailles de tous les Foréké-Dschang autour du sport, notre culture surtout que depuis
                                    la disparition de Fespaq il
                                    n'y plus eu un événement aussi important pour permettre aux Foréké-Dschang de se retrouver
                                    autour d’un même idéal.
                                </p>
                            </div>
                        </div><!-- 1st Latest Post end -->
        
                    </div>
                    <!--/ End Recent Posts-->
        
        
            <div class="col-md-4 col-sm-12 footer-widget">
	        <h3 class="widget-title">Actualités</h3>
            @forelse ($blogs as $blog)
	        <div class="latest-post-items media">
	          <div class="latest-post-content media-body">
	            <h4><a href="{{ route('blog-details', [$blog->slug]) }}">{{$blog->title}}</a></h4>
	          </div>
              
	        </div><!-- 1st Latest Post end -->
            @endforeach

	       
	      </div>
                    <!--/ end flickr -->
        
                    <div class="col-md-3 col-sm-12 footer-widget footer-about-us">
                        <h3 class="widget-title">Fonctionnalites</h3>
                        <a class="dropdown-item post-meta" href="{{ url('course/presentation') }}">La course</a>
                        <a class="dropdown-item post-meta" href="{{ url('bon-a-savoir1') }}">Infos pratiques</a>
                        <a class="dropdown-item post-meta" href="{{ url('inscription1') }}">S'inscrire</a>
                        <a class="dropdown-item post-meta" href="{{ url('faq') }}">Partenaires</a>
                        <a class="dropdown-item post-meta" href="{{ url('faq') }}">FAQ</a>
                    </div>
                    <!--/ end about us -->
        
                </div><!-- Row end -->
            </div><!-- Container end -->
        </footer>
        <!-- Footer end -->
        
        <!-- Copyright start -->
        <section id="copyright" class="copyright angle">
            <div class="container">
                <!-- <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="footer-social unstyled">
                            <li>
                                <a title="Facebook" href="#">
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
                                </a>
                                <a title="Google+" href="#">
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-youtube"></i></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div> -->
                <!--/ Row end -->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="copyright-info">
                            &copy; 2020 | Site officiel de l’Ascension de la Falaise de Foréké-Dschang <span>Designed by <a
                                    href="https://visibilitycam.com">VISIBILITY_CAM SARL.</a></span>
                        </div>
                    </div>
                </div>
                <!--/ Row end -->
                <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
                    <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
                </div>
            </div>
            <!--/ Container end -->
        </section>
        <!--/ Copyright end -->
        </div>
        <!-- Body inner end -->
<!-- jQuery -->
<script src="/assets/frontupdate/plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="/assets/frontupdate/plugins/bootstrap/bootstrap.min.js"></script>
<!-- Style Switcher -->
<script type="text/javascript" src="/assets/frontupdate/plugins/style-switcher.js"></script>
<!-- Owl Carousel -->
<script type="text/javascript" src="/assets/frontupdate/plugins/owl/owl.carousel.js"></script>
<!-- PrettyPhoto -->
<script type="text/javascript" src="/assets/frontupdate/plugins/jquery.prettyPhoto.js"></script>
<!-- Bxslider -->
<script type="text/javascript" src="/assets/frontupdate/plugins/flex-slider/jquery.flexslider.js"></script>
<!-- CD Hero slider -->
<script type="text/javascript" src="/assets/frontupdate/plugins/cd-hero/cd-hero.js"></script>
<!-- Isotope -->
<script type="text/javascript" src="/assets/frontupdate/plugins/isotope.js"></script>
<script type="text/javascript" src="/assets/frontupdate/plugins/ini.isotope.js"></script>
<!-- Wow Animation -->
<script type="text/javascript" src="/assets/frontupdate/plugins/wow.min.js"></script>
<!-- Eeasing -->
<script type="text/javascript" src="/assets/frontupdate/plugins/jquery.easing.1.3.js"></script>
<!-- Counter -->
<script type="text/javascript" src="/assets/frontupdate/plugins/jquery.counterup.min.js"></script>
<!-- Waypoints -->
<script type="text/javascript" src="/assets/frontupdate/plugins/waypoints.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
<script src="/assets/frontupdate/plugins/google-map/gmap.js"></script>

<!-- Main Script -->
<script src="/assets/frontupdate/js/script.js"></script>
<script src="/assets/frontupdate/js/final-countdown.js"></script>
<script src="/assets/frontupdate/js/main.js"></script>
<!-- <script src="/assets/frontupdate/js/jquery.countdown.js"></script>
 --><script src="/assets/frontupdate/js/cd_config.js"></script>
<script src="/assets/frontupdate/js/modernizr.custom.69142.js"></script>

    </body>
</html>
