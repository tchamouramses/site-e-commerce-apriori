<div class="header_section">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="logo"><a href="#"><img src="images/logo.png"></a></div>
            </div>
            <div class="col-sm-9">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Basculer la navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav">
                            <a class="nav-item nav-link" href="index.html">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Domicile</font>
                                </font>
                            </a>
                            <a class="nav-item nav-link" href="collection.html">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Collection</font>
                                </font>
                            </a>
                            <a class="nav-item nav-link" href="shoes.html">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Chaussures</font>
                                </font>
                            </a>
                            <a class="nav-item nav-link" href="racing boots.html">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Bottes de course</font>
                                </font>
                            </a>
                            <a class="nav-item nav-link" href="contact.html">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Contacter</font>
                                </font>
                            </a>
                            <a class="nav-item nav-link last" href="#"><img src="images/search_icon.png"></a>
                            <a class="nav-item nav-link last" href="contact.html"><img src="images/shop_icon.png"></a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="banner_section">
        <div class="container-fluid">
            <section class="slide-wrapper">
                <div class="container-fluid">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                            <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                            <li data-target="#myCarousel" data-slide-to="3" class=""></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-sm-2 padding_0">
                                        <p class="mens_taital">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">Hommes Chaussures</font>
                                            </font>
                                        </p>
                                        <div class="page_no">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">0/2</font>
                                            </font>
                                        </div>
                                        <p class="mens_taital_2">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">Hommes Chaussures</font>
                                            </font>
                                        </p>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="banner_taital">
                                            <h1 class="banner_text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Nouvelles chaussures de course </font>
                                                </font>
                                            </h1>
                                            <h1 class="mens_text"><strong>
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Hommes comme Plex</font>
                                                    </font>
                                                </strong></h1>
                                            <p class="lorem_text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</font>
                                                </font>
                                            </p>
                                            <button class="buy_bt">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Acheter maintenant</font>
                                                </font>
                                            </button>
                                            <button class="more_bt">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Voir plus</font>
                                                </font>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="shoes_img"><img src="images/running-shoes.png"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-sm-2 padding_0">
                                        <p class="mens_taital">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">Hommes Chaussures</font>
                                            </font>
                                        </p>
                                        <div class="page_no">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">0/2</font>
                                            </font>
                                        </div>
                                        <p class="mens_taital_2">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">Hommes Chaussures</font>
                                            </font>
                                        </p>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="banner_taital">
                                            <h1 class="banner_text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Nouvelles chaussures de course </font>
                                                </font>
                                            </h1>
                                            <h1 class="mens_text"><strong>
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Hommes comme Plex</font>
                                                    </font>
                                                </strong></h1>
                                            <p class="lorem_text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</font>
                                                </font>
                                            </p>
                                            <button class="buy_bt">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Acheter maintenant</font>
                                                </font>
                                            </button>
                                            <button class="more_bt">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Voir plus</font>
                                                </font>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="shoes_img"><img src="images/running-shoes.png"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-sm-2 padding_0">
                                        <p class="mens_taital">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">Hommes Chaussures</font>
                                            </font>
                                        </p>
                                        <div class="page_no">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">0/2</font>
                                            </font>
                                        </div>
                                        <p class="mens_taital_2">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">Hommes Chaussures</font>
                                            </font>
                                        </p>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="banner_taital">
                                            <h1 class="banner_text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Nouvelles chaussures de course </font>
                                                </font>
                                            </h1>
                                            <h1 class="mens_text"><strong>
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Hommes comme Plex</font>
                                                    </font>
                                                </strong></h1>
                                            <p class="lorem_text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</font>
                                                </font>
                                            </p>
                                            <button class="buy_bt">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Acheter maintenant</font>
                                                </font>
                                            </button>
                                            <button class="more_bt">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Voir plus</font>
                                                </font>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="shoes_img"><img src="images/running-shoes.png"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-sm-2 padding_0">
                                        <p class="mens_taital">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">Hommes Chaussures</font>
                                            </font>
                                        </p>
                                        <div class="page_no">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">0/2</font>
                                            </font>
                                        </div>
                                        <p class="mens_taital_2">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">Hommes Chaussures</font>
                                            </font>
                                        </p>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="banner_taital">
                                            <h1 class="banner_text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Nouvelles chaussures de course </font>
                                                </font>
                                            </h1>
                                            <h1 class="mens_text"><strong>
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Hommes comme Plex</font>
                                                    </font>
                                                </strong></h1>
                                            <p class="lorem_text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</font>
                                                </font>
                                            </p>
                                            <button class="buy_bt">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Acheter maintenant</font>
                                                </font>
                                            </button>
                                            <button class="more_bt">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Voir plus</font>
                                                </font>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="shoes_img"><img src="images/running-shoes.png"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>