@extends('layoutss/default')

{{-- slider --}}
@section('content')



<div class="collection_text">
    <font style="vertical-align: inherit;">
        <font style="vertical-align: inherit;">Collection</font>
    </font>
</div>
<div class="layout_padding collection_section">
    <div class="container">
        <h1 class="new_text"><strong>
                <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Nouvelle collection</font>
                </font>
            </strong></h1>
        <p class="consectetur_text">
            <font style="vertical-align: inherit;">
                <font style="vertical-align: inherit;">consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </font>
                <font style="vertical-align: inherit;">Ut enim ad minim veniam, quis nostrud exercice</font>
            </font>
        </p>
        <div class="collection_section_2">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-img">
                        <button class="new_bt">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Nouveau</font>
                            </font>
                        </button>
                        <div class="shoes-img"><img src="images/shoes-img1.png"></div>
                        <p class="sport_text">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Hommes Sports</font>
                            </font>
                        </p>
                        <div class="dolar_text">
                            <font style="vertical-align: inherit;"><strong style="color: #f12a47;">
                                    <font style="vertical-align: inherit;">90</font>
                                </strong>
                                <font style="vertical-align: inherit;"> $</font>
                            </font><strong style="color: #f12a47;">
                                <font style="vertical-align: inherit;"></font>
                            </strong>
                        </div>
                        <div class="star_icon">
                            <ul>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                            </ul>
                        </div>
                    </div>
                    <button class="seemore_bt">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Voir plus</font>
                        </font>
                    </button>
                </div>
                <div class="col-md-6">
                    <div class="about-img2">
                        <div class="shoes-img2"><img src="images/shoes-img2.png"></div>
                        <p class="sport_text">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Hommes Sports</font>
                            </font>
                        </p>
                        <div class="dolar_text">
                            <font style="vertical-align: inherit;"><strong style="color: #f12a47;">
                                    <font style="vertical-align: inherit;">90</font>
                                </strong>
                                <font style="vertical-align: inherit;"> $</font>
                            </font><strong style="color: #f12a47;">
                                <font style="vertical-align: inherit;"></font>
                            </strong>
                        </div>
                        <div class="star_icon">
                            <ul>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop