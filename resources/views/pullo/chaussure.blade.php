@extends('layoutss/default')

{{-- slider --}}
@section('content')



<div class="collection_text">
    <font style="vertical-align: inherit;">
        <font style="vertical-align: inherit;">Chaussures</font>
    </font>
</div>
<div class="collection_section layout_padding">
    <div class="container">
        <h1 class="new_text"><strong>
                <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Nouveautés Produits</font>
                </font>
            </strong></h1>
        <p class="consectetur_text">
            <font style="vertical-align: inherit;">
                <font style="vertical-align: inherit;">consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </font>
                <font style="vertical-align: inherit;">Ut enim ad minim veniam, quis nostrud exercice</font>
            </font>
        </p>
    </div>
</div>
<div class="layout_padding gallery_section">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="best_shoes">
                    <p class="best_text">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Meilleures chaussures </font>
                        </font>
                    </p>
                    <div class="shoes_icon"><img src="images/shoes-img4.png"></div>
                    <div class="star_text">
                        <div class="left_part">
                            <ul>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                            </ul>
                        </div>
                        <div class="right_part">
                            <div class="shoes_price">
                                <font style="vertical-align: inherit;"><span style="color: #ff4e5b;">
                                        <font style="vertical-align: inherit;">60</font>
                                    </span>
                                    <font style="vertical-align: inherit;"> $</font>
                                </font><span style="color: #ff4e5b;">
                                    <font style="vertical-align: inherit;"></font>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="best_shoes">
                    <p class="best_text">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Meilleures chaussures </font>
                        </font>
                    </p>
                    <div class="shoes_icon"><img src="images/shoes-img5.png"></div>
                    <div class="star_text">
                        <div class="left_part">
                            <ul>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                            </ul>
                        </div>
                        <div class="right_part">
                            <div class="shoes_price">
                                <font style="vertical-align: inherit;"><span style="color: #ff4e5b;">
                                        <font style="vertical-align: inherit;">400</font>
                                    </span>
                                    <font style="vertical-align: inherit;"> $</font>
                                </font><span style="color: #ff4e5b;">
                                    <font style="vertical-align: inherit;"></font>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="best_shoes">
                    <p class="best_text">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Meilleures chaussures </font>
                        </font>
                    </p>
                    <div class="shoes_icon"><img src="images/shoes-img6.png"></div>
                    <div class="star_text">
                        <div class="left_part">
                            <ul>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                            </ul>
                        </div>
                        <div class="right_part">
                            <div class="shoes_price">
                                <font style="vertical-align: inherit;"><span style="color: #ff4e5b;">
                                        <font style="vertical-align: inherit;">50</font>
                                    </span>
                                    <font style="vertical-align: inherit;"> $</font>
                                </font><span style="color: #ff4e5b;">
                                    <font style="vertical-align: inherit;"></font>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="best_shoes">
                    <p class="best_text">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Chaussures de sport</font>
                        </font>
                    </p>
                    <div class="shoes_icon"><img src="images/shoes-img7.png"></div>
                    <div class="star_text">
                        <div class="left_part">
                            <ul>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                            </ul>
                        </div>
                        <div class="right_part">
                            <div class="shoes_price">
                                <font style="vertical-align: inherit;"><span style="color: #ff4e5b;">
                                        <font style="vertical-align: inherit;">70</font>
                                    </span>
                                    <font style="vertical-align: inherit;"> $</font>
                                </font><span style="color: #ff4e5b;">
                                    <font style="vertical-align: inherit;"></font>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="best_shoes">
                    <p class="best_text">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Chaussures de sport</font>
                        </font>
                    </p>
                    <div class="shoes_icon"><img src="images/shoes-img8.png"></div>
                    <div class="star_text">
                        <div class="left_part">
                            <ul>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                            </ul>
                        </div>
                        <div class="right_part">
                            <div class="shoes_price">
                                <font style="vertical-align: inherit;"><span style="color: #ff4e5b;">
                                        <font style="vertical-align: inherit;">100</font>
                                    </span>
                                    <font style="vertical-align: inherit;"> $</font>
                                </font><span style="color: #ff4e5b;">
                                    <font style="vertical-align: inherit;"></font>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="best_shoes">
                    <p class="best_text">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Chaussures de sport</font>
                        </font>
                    </p>
                    <div class="shoes_icon"><img src="images/shoes-img9.png"></div>
                    <div class="star_text">
                        <div class="left_part">
                            <ul>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                                <li><a href="#"><img src="images/star-icon.png"></a></li>
                            </ul>
                        </div>
                        <div class="right_part">
                            <div class="shoes_price">
                                <font style="vertical-align: inherit;"><span style="color: #ff4e5b;">
                                        <font style="vertical-align: inherit;">90</font>
                                    </span>
                                    <font style="vertical-align: inherit;"> $</font>
                                </font><span style="color: #ff4e5b;">
                                    <font style="vertical-align: inherit;"></font>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="buy_now_bt">
            <button class="buy_text">
                <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Acheter maintenant</font>
                </font>
            </button>
        </div>
    </div>
</div>
@stop