<div class="table-responsive">
    <table class="table" id="imageProducts-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Image</th>
                <th>Product Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($imageProducts as $imageProduct)
            <tr>
                <td>{{ $imageProduct->name }}</td>
                <td><img height="50" src="{{ $imageProduct->image }}" alt=""></td>
                <td>{{ $imageProduct->product_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['imageProducts.destroy', $imageProduct->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('imageProducts.show', [$imageProduct->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('imageProducts.edit', [$imageProduct->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>