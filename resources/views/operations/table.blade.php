<div class="table-responsive">
    <table class="table" id="operations-table">
        <thead>
        <tr>
            <th>Name</th>
        <th>Description</th>
        <th>Type Operation Id</th>
        <th>Created At</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($operations as $operation)
            <tr>
                <td>{{ $operation->name }}</td>
            <td>{{ $operation->description }}</td>
            <td>{{ $operation->type_operation_id }}</td>
            <td>{{ $operation->created_at }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['operations.destroy', $operation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('operations.show', [$operation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('operations.edit', [$operation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
