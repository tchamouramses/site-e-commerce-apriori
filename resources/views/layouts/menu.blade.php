<li class="nav-item">
    <a href="{{ route('categories.index') }}"
       class="nav-link {{ Request::is('categories*') ? 'active' : '' }}">
        <p>Categories</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('typeProducts.index') }}"
       class="nav-link {{ Request::is('typeProducts*') ? 'active' : '' }}">
        <p>Type Products</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('products.index') }}"
       class="nav-link {{ Request::is('products*') ? 'active' : '' }}">
        <p>Products</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('typeOperations.index') }}"
       class="nav-link {{ Request::is('typeOperations*') ? 'active' : '' }}">
        <p>Type Operations</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('operations.index') }}"
       class="nav-link {{ Request::is('operations*') ? 'active' : '' }}">
        <p>Operations</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('operationProducts.index') }}"
       class="nav-link {{ Request::is('operationProducts*') ? 'active' : '' }}">
        <p>Operation Products</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('images.index') }}"
       class="nav-link {{ Request::is('images*') ? 'active' : '' }}">
        <p>Images</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('imageProducts.index') }}"
       class="nav-link {{ Request::is('imageProducts*') ? 'active' : '' }}">
        <p>Image Products</p>
    </a>
</li>


