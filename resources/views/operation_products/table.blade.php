<div class="table-responsive">
    <table class="table" id="operationProducts-table">
        <thead>
        <tr>
            <th>Prix</th>
        <th>Quantite</th>
        <th>Description</th>
        <th>Operation Id</th>
        <th>Product Id</th>
        <th>Created At</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($operationProducts as $operationProduct)
            <tr>
                <td>{{ $operationProduct->prix }}</td>
            <td>{{ $operationProduct->quantite }}</td>
            <td>{{ $operationProduct->description }}</td>
            <td>{{ $operationProduct->operation_id }}</td>
            <td>{{ $operationProduct->product_id }}</td>
            <td>{{ $operationProduct->created_at }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['operationProducts.destroy', $operationProduct->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('operationProducts.show', [$operationProduct->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('operationProducts.edit', [$operationProduct->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
